import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Controller {
    static Stage window;
    static Scene auth, createAccount, loggedIn;
    static Parent pAuth, pCreateAccount, pLoggedIn;

    static String username;

    public static void setWindow(ActionEvent event) {
        window = (Stage)((Node)event.getSource()).getScene().getWindow();
    }
}
