import Database.DB;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;

public class CreateAccount extends Application {
    public TextField username_field;
    public PasswordField password_field;
    public PasswordField confirm_password_field;
    public TextField email_field;

    public CheckBox accept_checkbox;
    public Button create_account_button;
    public Label username_err, password_err, confirm_err, email_err, accept_err, err_msg;

    public void onClickReturnHyperlink(ActionEvent event) throws IOException {
        Controller.setWindow(event);
        Controller.window.setScene(Controller.auth);
    }

    @Override
    public void init() {
        System.out.printf("CreateAccount:init%n");
    }

    @Override
    public void start(Stage stage) throws Exception {
    }

    public void clearWarnings() {
        username_err.setText("");
        password_err.setText("");
        confirm_err.setText("");
        email_err.setText("");
        accept_err.setText("");
        err_msg.setText("");
    }

    public void onClickCreateAccountButton(ActionEvent actionEvent) throws SQLException {
        clearWarnings();

        // login nie może zawierać znaków specjalnych spacji !@#%^$&%(*)

        if (username_field.getText().length() == 0 || password_field.getText().length() == 0 || confirm_password_field.getText().length() == 0 || email_field.getText().length() == 0) {
            // nie wszystkie pola są wypełnione
            if (username_field.getText().length() == 0) username_err.setText("X");
            if (password_field.getText().length() == 0) password_err.setText("X");
            if (confirm_password_field.getText().length() == 0) confirm_err.setText("X");
            if (email_field.getText().length() == 0) email_err.setText("X");

            err_msg.setText("Nie wszystkie pola są wypełnione");
            return;
        }
        if (password_field.getText().compareTo(confirm_password_field.getText()) != 0) {
            // hasla sie nie zgadzają
            password_err.setText("X");
            confirm_err.setText("X");
            err_msg.setText("Hasła się nie zgadzają");
            return;
        }
        if (!accept_checkbox.isSelected()) {
            // nie zaakceptowano postanowień
            accept_err.setText("X");
            err_msg.setText("Nie zaakceptowałeś postanowień");
            return;
        }
        if (DB.getOriginalValue(username_field.getText(), 2).length() != 0) {
            // uzytkownik juz istnieje
            username_err.setText("X");
            err_msg.setText("Nazwa użytkownika już istnieje");
            return;
        }
        if (DB.getOriginalValue(email_field.getText(), 4).length() != 0) {
            // adres email jest juz uzywany
            email_err.setText("X");
            err_msg.setText("Adres email jest już przypisany do innego konta");
            return;
        }

        LocalDateTime creationDate = LocalDateTime.now();
        Timestamp timestamp = Timestamp.valueOf(creationDate);

        System.out.printf("%d %s %s %s%n", DB.getNextID(), username_field.getText(), password_field.getText(), email_field.getText());

        if (DB.addRow(2, DB.getNextID(), username_field.getText(), password_field.getText(), email_field.getText(), timestamp)) {
            System.out.printf("Utworzono konto%n");
            err_msg.setText("");
        } else {
            System.out.printf("Nie udało się stworzyć konta%n");
        }
    }

    public boolean usernameFieldUpdate(InputMethodEvent keyEvent) throws SQLException {
        if (DB.getOriginalValue(username_field.getText(), 2).length() != 0) {
            username_err.setText("X");
            err_msg.setText("Nazwa użytkownika już istnieje");
        }
        else if (username_field.getText().length() == 0) {
            username_err.setText("");
            err_msg.setText("");
        }
        else if (username_field.getText().length() <= 3) {
            username_err.setText("X");
            err_msg.setText("Nazwa użytkownika jest za krótka");
        }
        else {
            username_err.setText("");
            err_msg.setText("");
            return true;
        }

        return false;
    }

    public boolean emailFieldUpdate(KeyEvent keyEvent) throws SQLException {
        if (DB.getOriginalValue(email_field.getText(), 4).length() != 0) {
            username_err.setText("X");
            err_msg.setText("Adres email jest już przypisany do innego konta");
        }
        else if (email_field.getText().length() == 0) {
            username_err.setText("");
            err_msg.setText("");
        }
        else if (!email_field.getText().contains("@") || !email_field.getText().contains(".")) {
            username_err.setText("X");
            err_msg.setText("Wpisz prawidłowy adres email, przykład: email@example.com");
        }
        else {
            username_err.setText("");
            err_msg.setText("");
            return true;
        }

        return false;
    }
}
