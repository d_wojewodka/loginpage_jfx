package Database;

import java.sql.*;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

public class DB {
    private static String url = "jdbc:mysql://localhost:3306/?serverTimezone=UTC"; // localhost is ip address
    private static String user = "user";
    private static String password = "password";

    private static Connection con = null;
    private static PreparedStatement pst = null;
    private static ResultSet rs = null;
    private static ResultSetMetaData rsmd = null;
    private static Statement st = null;

    private static String schema = "";
    private static String table = "";

    public DB() {}

    public static void connect() throws SQLException{
        try {
            System.out.printf("Łączenie z bazą danych...%n");
            con = DriverManager.getConnection(url, user, password);
            System.out.printf("Nawiązano połaczenia z bazą danych!%n");
            System.out.printf("");
        }
        catch (SQLException ex) {
            System.err.println("SQLException: " + ex.getMessage());
            System.err.println("SQLState: " + ex.getSQLState());
            System.err.println("VendorError: " + ex.getErrorCode());
        }
    }
    public static Connection getCon() { return con; }

    public static void selectDatabase(String s, String t) {
        schema = s;
        table = t;
    }

    public static void showAllTables() throws SQLException {
        DatabaseMetaData metaData = con.getMetaData();
        String[] types = {"TABLE"};

        ResultSet tables = metaData.getTables(null, null, "%", types);

        while (tables.next()) {
            System.out.printf("> %s%n", tables.getString("TABLE_NAME"));
        }
    }

    public static boolean auth(String user, String password) throws SQLException {
        st = con.createStatement();
        rs = st.executeQuery("SELECT * FROM " + schema + "." + table);
        rsmd = rs.getMetaData();

        while (rs.next()) {
            if (rs.getString(2).toLowerCase().compareTo(user.toLowerCase()) == 0)
            {
                if (rs.getString(3).compareTo(password) == 0) {
                    System.out.printf("%nZalogowano jako %s (id %s)%n", rs.getString(2), rs.getString(1));
                    return true;
                }
            }
        }

        return false;
    }

    public static int getNextID() throws SQLException {
        st = con.createStatement();
        rs = st.executeQuery("SELECT * FROM " + schema + "." + table);
        rsmd = rs.getMetaData();

        int col = rsmd.getColumnCount();
        int nextID = 0;

        while (rs.next()) {
            if (rs.getInt(1) > nextID)
                nextID = rs.getInt(1);
        }

        return nextID+1;
    }

    public static String getOriginalValue(String value, Integer column) throws SQLException {
        st = con.createStatement();
        rs = st.executeQuery("SELECT * FROM " + schema + "." + table);
        rsmd = rs.getMetaData();

        int col = rsmd.getColumnCount();

        while (rs.next()) {
            if (rs.getString(column).toLowerCase().compareTo(value.toLowerCase()) == 0) {
                return rs.getString(column);
            }
        }

        return "";
    }

    public static boolean addRow(int primaryKey, Object... values) throws SQLException {
        st = con.createStatement();
        rs = st.executeQuery("SELECT * FROM " + schema + "." + table);
        rsmd = rs.getMetaData();

        int col = rsmd.getColumnCount();
        String cells = "";

        if (col != values.length) {
            System.err.printf(">addRow< Parameter count mismatch, required %d, given %d%n", col, values.length);
            return false;
        }

        for (int i = 0; i < values.length; i++) {
            cells += "?";
            if (i < values.length - 1)
                cells += ",";
        }

        String sql = String.format("INSERT INTO %s.%s values(%s)", schema, table, cells);
        pst = con.prepareStatement(sql);


        // checking duplication for primary key
        while (rs.next()) {
            if (rs.getString(primaryKey).compareTo((String) values[primaryKey-1]) == 0) {
                System.err.printf(">addRow< PrimaryKey duplication found at column %d%n", primaryKey);
                return false;
            }
        }

        // autoboxing value type
        for (int i = 0; i < values.length; i++) {
            if (values[i] instanceof String) {
                // in MySQL column index are counted from 1 (in Java from 0)
                pst.setString(i+1, values[i].toString());
            } else if (values[i] instanceof Float) {
                pst.setFloat(i+1, ((Float) values[i]).floatValue());
            } else if (values[i] instanceof Double) {
                pst.setDouble(i+1, ((Double) values[i]).floatValue());
            } else if (values[i] instanceof Integer) {
                pst.setInt(i+1, ((Integer) values[i]).intValue());
            }  else if (values[i] instanceof Timestamp) {
                pst.setTimestamp(i+1, Timestamp.valueOf(((Timestamp) values[i]).toLocalDateTime()));
            } else {
                System.err.printf(">addRow< Unsupported data type%n");
                return false;
            }
        }

        // executing update
        int rows = pst.executeUpdate();
        String s = "";

        if (rows > 0) {
            if (rows > 1) {
                s = "s";
            }
            System.out.printf(">addRow< Added %d row%s in %s/%s%n", rows, s, schema, table);
        }
        return true;
    }

    public static void exit() throws SQLException, InterruptedException {
        System.out.println("");
        System.out.println("Wyłączanie");
        System.out.println("3");
        TimeUnit.SECONDS.sleep(1);
        System.out.println("2");
        TimeUnit.SECONDS.sleep(1);
        System.out.println("1");
        TimeUnit.SECONDS.sleep(1);
        con.close();
    }

    public static TreeMap<Integer, Object[]>  tableToMap() throws SQLException {
        st = con.createStatement();
        rs = st.executeQuery("SELECT * FROM " + schema + "." + table);
        rsmd = rs.getMetaData();

        int col = rsmd.getColumnCount();

        // maps
        TreeMap<Integer, Object[]> map = new TreeMap<Integer, Object[]>();
        String[] nameRow = new String[col];

        for (int i = 0; i < col; i++) {
            nameRow[i] = rsmd.getColumnName(i+1);
        }

        // adding table column name
        map.put(0, nameRow);

        // row iteration
        Integer row = 1;

        while (rs.next()) {
            Object[] collectedRow = new String[col];
            for (int i = 0; i < col; i++) {
                collectedRow[i] = rs.getString(i+1);
            }

            map.put(row, collectedRow);
            row++;
        }

        return map;
    }

    public static void main(String... args) {}
}
