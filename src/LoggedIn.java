import javafx.application.Application;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Map;
import java.util.SimpleTimeZone;
import java.util.TreeMap;

import Database.*;

public class LoggedIn extends Application {
    public Label welcome_message;


    public TableView table;

    // TableView columns
    public TableColumn index, username, password, email, creationDate;


    @Override
    public void init() throws SQLException {
        System.out.printf("LoggedIn:init%n");
        welcome_message.setText(String.format("Witaj, %s!", Controller.username));

        TreeMap<Integer, Object[]> table = DB.tableToMap();
        for (Map.Entry<Integer, Object[]> entry : table.entrySet())
            System.out.printf("%d: %s%n",entry.getKey(), Arrays.toString(entry.getValue()));

    }

    @Override
    public void start(Stage stage) throws Exception { }

    public void onClickLogoutButton(ActionEvent event) throws IOException {
        //Controller.setWindow(event);
        Controller.window.setScene(Controller.auth);
    }
}
