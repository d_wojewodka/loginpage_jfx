import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.*;
import java.sql.*;

import Database.*;

public class Auth extends Application {
    public TextField username_field;
    public PasswordField password_field;
    public CheckBox save_login;
    public Hyperlink hyperlink_create_account;
    public Label error_message;

    @Override
    public void init() throws SQLException {
        System.out.printf("Auth:init%n");

        DB db = new DB();
        DB.connect();
        DB.selectDatabase("authentication", "login");
    }

    @Override
    public void start(Stage stage) throws Exception, IOException{
        Controller.window = stage;

        // auth GUI
        Parent authGUI = FXMLLoader.load(getClass().getResource("Auth.fxml"));
        Controller.auth = new Scene(authGUI, 600, 350);

        stage.setScene(Controller.auth);
        stage.show();
    }

    public static void main(String... args) {
        launch(args);
    }

    public void onClickLoginButton(ActionEvent event) throws SQLException, IOException {
        if (DB.auth(username_field.getText(), password_field.getText())) {
            error_message.setText("");

            Controller.username = DB.getOriginalValue(username_field.getText(), 2);
            username_field.clear();
            password_field.clear();

            // create account GUI
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("LoggedIn.fxml"));
            Controller.pLoggedIn = loader.load();

            Controller.loggedIn = new Scene(Controller.pLoggedIn, 600, 350);
            LoggedIn controller = loader.getController();
            controller.init();

            Controller.setWindow(event);
            Controller.window.setScene(Controller.loggedIn);
        }
        if (username_field.getText().length() == 0 || password_field.getText().length() == 0) {
            error_message.setText("Wypełnij wszystkie pola");
        }
        else {
            error_message.setText("Nieprawidłowa nazwa użytkownika lub hasło");
        }
    }


    public void onClickCreateAccountHyperlink(ActionEvent event) throws IOException {
        // create account GUI
        Parent createAccountGUI = FXMLLoader.load(getClass().getResource("CreateAccount.fxml"));
        Controller.createAccount = new Scene(createAccountGUI, 600, 350);

        Controller.setWindow(event);
        Controller.window.setScene(Controller.createAccount);
    }
}
